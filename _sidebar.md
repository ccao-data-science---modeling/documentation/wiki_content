[Home](Home)

##### The Data Department

* [Handbook](Handbook/Handbook)
* [Mission, Vision, and Values](Handbook/Mission-Vision-Values)
* [Resources](Handbook/Resources)
* [Glossary](Handbook/Glossary)
* [Not Public](Handbook/Not-Public)

##### People

* [Onboarding](People/Onboarding)
* [External Developer Engagement Program](People/Contributing)

##### How-To

* [Setup the Cook County VPN](How-To/Setup-the-Cook-County-VPN)
* [Setup the AWS Command Line Interface and Multi-factor Authentication](How-To/Setup-the-AWS-Command-Line-Interface-and-Multi-factor-Authentication)
* [Connect to AWS Resources](How-To/Connect-to-AWS-Resources)
* [Administer Users on CCAO Services](How-To/Administer-Users-on-CCAO-Services)

##### Standard Operating Procedures (SOPs)

* [Sales Ratio Studies](SOPs/Sales-Ratio-Studies)
* [Desk Review](SOPs/Desk-Review)
* [Open Data](SOPs/Open-Data)

##### Residential

* [Single and Multi-Family Residential Model](https://gitlab.com/ccao-data-science---modeling/models/ccao_res_avm)
* [Condominium Model](https://gitlab.com/ccao-data-science---modeling/models/ccao_condo_avm)
* [Residential Exemptions](Residential/Residential-Exemptions)
* [Home Improvement Exemptions (288s)](Residential/Home-Improvement-Exemptions)
* [Cook County Property Tax Ordinance](https://library.municode.com/il/cook_county/codes/code_of_ordinances?nodeId=PTIGEOR_CH74TA_ARTIIREPRTA)

##### RPIE

* [RPIE Overview](RPIE/Overview)
* [RPIE FAQs](RPIE/FAQs)
* [RPIE Privacy Policy](RPIE/Privacy-Policy)
* [RPIE Details](RPIE/What-To-Expect)
* [RPIE 2023 Form](RPIE/RPIE2023.pdf)

##### Data Documentation

*Packages*

* [AssessR R Package Documentation](https://ccao-data-science---modeling.gitlab.io/packages/assessr/reference/)
* [CCAO R Package Documentation](https://ccao-data-science---modeling.gitlab.io/packages/ccao/reference/)

*Office Terminology*

* [Property Class Definitions](Data/Class-Definitions.pdf) - *Available via the [CCAO R package](https://gitlab.com/ccao-data-science---modeling/packages/ccao)*
* [Township Definitions](Data/Townships) - *Available via the [CCAO R package](https://gitlab.com/ccao-data-science---modeling/packages/ccao)*
* [CDU Codes](Data/CDU Codes) - *Available via the [CCAO R package](https://gitlab.com/ccao-data-science---modeling/packages/ccao)*

*Data Guides*

* [RPIE SQL Database Data Dictionary](RPIE/rpie-data-dictionary.xlsx)
* [Data Catalog](Data/Data-Catalog.xlsx)
* [iasWorld Tables](Data/iasWorld-Tables.xlsx)
* [iasWorld Keys](Data/iasWorld-PK-FK-2021-06-14.xlsx) - *As of June 14, 2021*
* [iasWorld ERD (Preliminary Draft)](Data/iasWorld-ERD.pdf)
