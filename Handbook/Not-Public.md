The Data Department makes things [public by default](Handbook/Mission-Vision-Values#public-by-default). However, some things cannot be made public and are internal-only. Material can be withheld from the public for the following reasons:

* The material contains sensitive, identifying information, including Social Security Numbers.
* The material pertains to sensitive employment actions, including discipline. 
* The material is part of the deliberative process or is a draft of a to-be-public document.
* The material contains data or information that is licensed or proprietary (e.g. API results).

The following items are currently internal-only:

* **RPIE data:** Some RPIE data individually identifies and reveals the income and profits of commercial properties. These fields have been withheld from public data due to privacy concerns. 
* **Notes repository:** The CCAO maintains a [repository with notes from meetings and internal resources](https://gitlab.com/ccao-data-science---modeling/documentation/notes). This is not made public since most of the notes are part of the deliberative process and would be useless to members of the public.
* **WIP repositories:** Certain experimental repositories that may or may not be used are considered deliberative and are not made public.

